<div align="center">
  <img width="72" src="https://ae01.alicdn.com/kf/H25ff04ea30db4ad0abbcdd88ed4d84dbl.png"/>
  <h1>十源</h1>
  <p>🎉<code>极简</code> + <code>优雅</code>🎉</p>
</div>

---

网络导入链接（**_不支持阅读 3.0 哦_**）：

```html
https://gitee.com/ecruos/oo/raw/master/yuedu/source.json
```

> 提示更新“十源”时，再次导入该链接即可 (o˘◡˘o)

---

<div align="center">
  <h2>📚 收录源 · 一览</h2>
</div>

- **排行**

  - 整合多站的排行榜和首页推荐
  - 支持试读免费章节
  - 一般两三天更新一次“排行”数据

- **优书**

  - 换源时显示小说评分
  - 正文是小说评论

- **书单**

  - 优书书单
  - 支持搜索书名、书单名
  - 支持直接搜索正文内的书单 ID

- **豆书**

  - 豆瓣图书
  - 热门短评、笔记、摘抄、书评

- **龙空**

  - 龙空论坛

- **知轩**

  - 知轩藏书，精校小说

- **起点**

- **爱书吧**、**思路客**、**九桃**、**斋书苑**、**妙笔阁**、**笔趣库**

---

<div align="center">
  <h2>🖼️ 效果 · 一览</h2>
</div>

> 如果看不到图片，说明你的浏览器拦截了阿里的图片（`ae01.alicdn.com`）

![发现页](https://ae01.alicdn.com/kf/Ha3f89655cf264a17911c81d1676b1a442.jpg)

---

![搜索](https://ae01.alicdn.com/kf/H0adaf7a1ba094c408f03159f1ce4c85aw.jpg)

---

![排行 - 女频 - 热榜](https://ae01.alicdn.com/kf/H122e025eb5274c64a1d31b57a6acb3e9Z.jpg)

---

![书单](https://ae01.alicdn.com/kf/Hc54dec351c294662abca6ad8de7d382dy.jpg)

---

![书单id](https://ae01.alicdn.com/kf/H374a81a347c14818a3e77eb062f3e99dM.jpg)

---

![换源](https://ae01.alicdn.com/kf/H0ce439a1136e4607b3c22092686421ac6.jpg)

---

![豆书](https://ae01.alicdn.com/kf/H401da92a0ee74de79631feb7f6abf8f6B.jpg)

---

![龙空](https://ae01.alicdn.com/kf/Hb8af209bbe964fec882b8fb4b0335f31N.jpg)

---
