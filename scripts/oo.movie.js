// ==UserScript==
// @name         oo.movie（ VIP视频原网页解析 + 精选在线观看源 ）
// @name:en      oo.movie
// @version      20.3.13
// @description  精选VIP视频解析和在线观看源，视频广告少，原网页解析，享受VIP的原画观影体验。支持腾讯视频、爱奇艺、优酷、芒果TV、搜狐视频、PPTV、1905、乐视等网站。VIP解析评分排序，不同网站不同排序，越好用的解析越靠前。
// @description:en  NO VIP NO VIDEO. For v.qq, iqiyi, youku, mgtv, tv.sohu, pptv, 1905, letv.
// @author       (o˘◡˘o)
// @namespace    https://gitee.com/ecruos/oo
// @updateURL    https://gitee.com/ecruos/oo/raw/master/scripts/oo.movie.js
// @supportURL   https://gitee.com/ecruos/oo
// @icon         https://p.pstatp.com/origin/fe690000c9141955bbac
// @license      GPL License
// @include      *
// ==/UserScript==

!async function() {
  var PLUGIN_ID = "(o˘◡˘o) oo.movie", PLUGIN_ATTR = "oo-movie", PLUGIN_NAME = "oo.movie", HTML = document.getElementsByTagName("html")[0];
  if (HTML.getAttribute(PLUGIN_ATTR) !== PLUGIN_ID) {
    var SEARCH_URLS = "豆瓣  https://search.douban.com/movie/subject_search?search_text=**  https://m.douban.com/search/?type=movie&query=**\n\n腾讯  https://m.v.qq.com/search.html?act=0&keyWord=**   https://v.qq.com/x/search/?q=**\n\n爱奇艺  https://m.iqiyi.com/search.html?source=default&key=**  https://so.iqiyi.com/so/q_**\n\n优酷  https://www.soku.com/m/y/video?q=**  https://so.youku.com/search_video/q_**\n\n芒果  https://m.mgtv.com/so/?k=**  https://so.mgtv.com/so/k-**\n\n哔哩哔哩  https://m.bilibili.com/search.html?keyword=**  https://search.bilibili.com/all?keyword=**\n\n1090  https://1090ys.com/?c=search&sort=addtime&order=desc&page=1&wd=**\n\n哔滴  https://bde4.com/search/**\n\n云播  https://m.yunbtv.com/vodsearch/-------------.html?wd=**  https://www.yunbtv.com/vodsearch/-------------.html?wd=**\n\n飞极速  http://m.feijisu8.com/search/**  http://feijisu8.com/search/**\n\nPPTV https://sou.pptv.com/s_video?kw=**  https://msou.pptv.com/s_video/pg_result?keyword=**\n\n搜狐  https://m.tv.sohu.com/upload/h5/m/mso.html?key=**  https://so.tv.sohu.com/mts?wd=**\n\n1905 https://vip.1905.com/Search?q=**\n\n乐视  https://m.le.com/search?wd=**  https://so.le.com/s?wd=**\n\n完美  https://www.wanmeikk.me/search/-------------.html?wd=**", VIP_URLS = "20.1 安歌 ￥ 1905,3,m | iqiyi,1,m | mgtv,5,m | pptv,5,m | sohu,1.8,m | v.qq,2.1,m | youku,2.2,m  https://pp.ntjb.cc/parse/?url=✔\n\n17.3 辰星 ￥ 1905,0.6 | iqiyi,1.8,m | le,1,m | mgtv,3.8,m | pptv,4.8,m | sohu,1,m | v.qq,2.3,m | youku,2,m  https://wilp.cc/?url=✔\n\n17.1 博衍 ￥ 1905,3,m | iqiyi,1,m | le,1,m | mgtv,4,m | pptv,3,m | v.qq,1 | youku,2.1,m  https://by.mgazc.com/?url=✔\n\n16.1 绎心 ￥ 1905,1,m | iqiyi,1,m | le,1,m | mgtv,4,m | pptv,3,m | v.qq,4,m | youku,2.1,m  https://v.hmptjgymbd.cn/v.php?url=✔\n\n16 南乔 ￥ iqiyi,4 | le,4 | mgtv,4 | v.qq,4  http://beijixs.cn/?url=\n\n14.5 斯年 ￥ 1905,1,m | iqiyi,1,m | le,1,m | mgtv,4,m | pptv,3,m | v.qq,4,m | youku,0.5,m  https://jx.pbahmbjwhmjz.cn/vip.php?url=✔\n\n14.4 兰采 ￥ 1905,0.6,m | iqiyi,1,m | le,1,m | mgtv,3,m | pptv,3,m | sohu,1.5,m | v.qq,2.2,m | youku,2.1,m  https://www.mjwzn.top/jx/api/?url=✔\n\n14.6 翼遥 ￥ 1905,0.1,m | iqiyi,1 | mgtv,5,m | sohu,1,m | v.qq,2.5,m | youku,5,m  https://www.upnld.vip/api.php?url=✔\n\n14.3 清和 ￥ 1905,1,m | iqiyi,1.5,m | le,1,m | mgtv,3,m | pptv,2,m | sohu,1.5,m | v.qq,2.2,m | youku,2.1  https://www.ukewm.com/?url=✔\n\n13.5 耀灵 ￥ 1905,1,m | iqiyi,2,m | le,1,m | mgtv,1,m | pptv,3,m | sohu,1.5,m | v.qq,4.5,m | youku,0.5,m  https://api.138us.com/jx/?url=✔\n\n12.4 景云 ￥ 1905,1,m | mgtv,4,m | pptv,3,m | v.qq,2.3,m | youku,2.1  https://www.wijnjfg.com/jx/api/jx.php?url=✔\n\n12.3 燕飞 ￥ mgtv,4,m | iqiyi,2,m | v.qq,3.3,m | youku,3,m  https://jiexi.009061.cc/jx.php?url=✔\n\n10.7 鹰扬 ￥ 1905,1,m | iqiyi,2.5,m | mgtv,0.5,m | pptv,1,m | sohu,1.2,m | v.qq,4,m | youku,0.5,m  https://jiexi.l192.com/?url=✔\n\n10.5 零露 ￥ 1905,1,m | iqiyi,1,m | mgtv,2,m | pptv,3,m | v.qq,4,m | youku,0.5,m  https://hmbsmfgtjzrd.cn/video.php?url=✔\n\n9.7 乐康 ￥ 1905,1.1,m | iqiyi,0.2,m | mgtv,3,m | sohu,1.2,m | v.qq,2.2 | youku,2,m  https://www.9v2nzbko.cn/jiexi.php?url=✔\n\n8.8 望舒 ￥ 1905,0.6,m | iqiyi,0.5,m | le,1,m | mgtv,2.5,m | sohu,1.2,m | v.qq,2.5,m,w | youku,0.5,m  https://www.mvz6060.com/jx/ty.php?url=✔\n\n8.8 巧颜 ￥ iqiyi,2.5 | mgtv,3.1 | v.qq,3.2,m  https://e83jz.cn/vip/vip.php?url=✔\n\n8.5 琼华 ￥ 1905,1,m | iqiyi,2,m | mgtv,0.5,m | sohu,1.5,m | v.qq,3,m | youku,0.5,m  https://jiexi.hj7na.cn/?url=✔\n\n15 胜蓝 ￥ 1905,1,m | iqiyi,4,m | le,3,m | miguvideo,1,m | mgtv,1.5,m | pptv,2,m | sohu,1 | v.qq,2,m | youku,3,m  https://z1.m1907.cn/?jx=", BETTER_ADDONS = [ {
      name: "哔哩哔哩·搜索",
      match: /bilibili.com\/search|search.bilibili.com/,
      jump: "#all-list | append, .index__board__src-search-board-"
    }, {
      name: "哔哩哔哩 - m",
      match: /m\.bilibili\.com/,
      sign: ".mg-footer-copyright",
      hide: ".index__openAppBtn__src-commonComponent-topArea-, .index__container__src-commonComponent-bottomOpenApp-, .bili-app, .recom-wrapper, .b-footer, .open-app-bar, .open-app-float, .more-review-wrapper"
    }, {
      name: "腾讯·搜索",
      match: /v.qq.com\/(\w+\/)?search/,
      jump: "#result, .wrapper_main > .mod_pages",
      sign: ".copyright",
      hide: ".tvp_app_bar"
    }, {
      name: "腾讯·播放页",
      match: /v\.qq\.com\/(cover|play|x\/cover|x\/page|x\/play|x\/m\/cover|x\/m\/page|x\/m\/play)/,
      vip: "#vip_title, .U_box_bg_a, .player_headline, .mod_video_info",
      title: ".mod_video_info .video_title, ._main_title, .player_title",
      fixUrl(n) {
        if (n.includes("cid=")) {
          var o = n.match(/cid=(\w+)/)[1], e = n.match(/vid=(\w+)/);
          return e = e ? "/" + e[1] : "", "https://v.qq.com/x/cover/".concat(o).concat(e, ".html");
        }
        return n.includes("/x/cover") ? n.replace(/\.html.*/, ".html") : n;
      },
      hide: '.mod_source, .video_function, .mod_promotion, #vip_privilege, #vip_activity, .U_bg_b, .btn_open_v, .btn_openapp, #vip_header, .btn_user_hd, .mod_sideslip_privileges, .mod_game_rec, .mod_source, .mod_promotion, .mod_sideslip_h, .btn_open, .btn_pay, .mod_box_lastview, .mod_vip_popup, .mod_vip_popup + .mask_layer, txpdiv[data-role="hd-ad-adapter-interactivelayer"]',
      css: "\nbody, #vip_title {\n  padding-bottom: 0 !important;\n}\n\n.mod_episodes_numbers.is-vip .item {\n  width: auto;\n  padding: 0 1em;\n}\n\n.U_html_bg .container {\n  padding-bottom: 30px;\n}\n\n.mod_play .mod_player_viptips .btn_try {\n  left: 30%;\n}"
    }, {
      name: "爱奇艺·搜索",
      match: /m.iqiyi.com\/search|so.iqiyi.com/,
      jump: "-.m-box, .search-con-page",
      sign: ".m-footer",
      hide: ".btn-ticket, .btn-yuyue, .btn-download, .m-iqyDown"
    }, {
      name: "爱奇艺·播放页",
      match: /\.iqiyi\.com\/v_/,
      vip: 'div[name="m-videoInfo"], #block-C',
      title: "#widget-videotitle, .video-title, .c-title-link, .player-title a",
      fixUrl: !0,
      sign: ".m-footer",
      hide: '.m-iqyDown, .header-login + div, .m-video-action, div[name="m-vipRights"], div[name="m-extendBar"], .m-iqylink-diversion, .m-iqylink-guide, .c-openVip, .c-score-btn, .m-videoUser-spacing, .m-pp-entrance, .m-hotWords-bottom, div[template-type="ALBUM"] .m-player-tip, .iqp-box-integral, .oo-iframes ~ div[style], .oo-iframes ~ div[id]',
      css: '\n.page_play {\n  padding-bottom: 0;\n}\n\ndiv[name="m-videoInfo"] {\n  padding-top: 1em;\n}\n\n.m-box-items .oo-album-item {\n  border-radius: 0.05em;\n  background-color: #e9ecef;\n  color: #495057;\n  padding: 0.5em 1em;\n  display: inline-flex;\n  justify-content: center;\n  align-items: center;\n  margin: 0.25em;\n  font-weight: bold;\n}\n\n.m-video-player #oo-player-iframe {\n  padding-top: 56.25%;\n  top: 50%;\n  transform: translateY(-50%);\n}\n',
      js() {
        log("修复爱奇艺选集");
        var n = [], o = ($('[name="apple-itunes-app"]').attr("content") || "").match(/aid=\d{2,}/);
        if (o) {
          fetch("https://pcw-api.iqiyi.com/albums/album/avlistinfo?page=1&size=9999&" + o[0]).then((function(n) {
            return n.json();
          })).then((function(o) {
            n = o.data.epsodelist;
          }));
          var e = 0;
          setInterval((function() {
            var o = Number($(".qy-episode-num .select-item.selected .select-link").text() || 0);
            if (o && o !== e) {
              log("change episode num: ".concat(e, " → ").concat(o));
              var i = n[(e = o) - 1];
              if (i) {
                var t = i.playUrl.replace(/https?:/, location.protocol);
                t !== ooPlayUrl && (ooPlayUrl = t, log("change episode to:", t));
              }
            }
          }), 100);
        }
      }
    }, {
      name: "优酷·搜索",
      match: /soku.com\/m.+q=|so.youku.com\/search_video/,
      jump: "#bpmodule-main, .yk_result"
    }, {
      name: "优酷·播放页",
      match: /m\.youku\.com\/a|m\.youku\.com\/v|v\.youku\.com\/v_/,
      vip: ".h5-detail-info, .player-title",
      title: ".player-title .subtitle a, .module-name, .anthology-title-wrap .title, .title-link",
      fixUrl: !0,
      sign: ".copyright",
      hide: ".h5-detail-guide, .h5-detail-ad, .brief-btm, .smartBannerBtn, .cmt-user-action, #right-title-ad-banner, .Corner-container",
      css: "\n#bpmodule-playpage-lefttitle {\n  height: auto !important;\n}"
    }, {
      name: "土豆·播放页",
      match: /\.tudou.com\/v\//,
      vip: ".play-video-desc, .td-play__baseinfo",
      title: ".td-listbox__title, .video-desc-title",
      fixUrl: !0,
      hide: ".video-player-topbar, .td-h5__player__appguide, #tudou-footer, .dropdown__panel__con"
    }, {
      name: "芒果·搜索",
      match: /m.mgtv.com\/so\/|so.mgtv.com\/so/,
      jump: "#paginator, .result-box .media",
      keyword: /k[-=]([^&\?\/\.]+)/
    }, {
      name: "芒果·播放页",
      match: /\.mgtv\.com\/(b|l)\//,
      vip: [ ".xuanji | before", ".v-panel-box" ],
      title: ".v-panel-title, .vt-txt",
      fixUrl: !0,
      sign: ".mg-footer-copyright",
      hide: ".ad-banner, .video-area-bar, .video-error .btn, .m-vip-list, .m-vip-list + div:not([class]), .toapp, .video-comment .ft, .mg-app-swip"
    }, {
      name: "搜狐·搜索",
      match: /m.tv.sohu.com.+key=|so.tv.sohu.com.+wd=/,
      jump: ".ssMore | before, .select-container | before"
    }, {
      name: "搜狐·播放页",
      match: /film\.sohu\.com\/album\/|tv\.sohu\.com\/(v|phone_play_film)/,
      vip: ".title-wrap, .videoInfo, .tw-info, .player-detail, .movie-info-content",
      title: "#vinfobox h2, .t-info, .movie-t h3",
      fixUrl(n) {
        if (/phone_play_film.+channeled=/.test(n)) {
          var o = n.match(/channeled=(\w+)/)[1], e = n.match(/aid=(\w+)/)[1];
          return "https://film.sohu.com/album/".concat(e, ".html?channeled=").concat(o);
        }
        return n;
      },
      sign: ".links",
      hide: ".actv-banner, .btn-xz-app, .twinfo_iconwrap, .btn-comment-app, #ad_banner, .advertise, .main-ad-view-box, .foot.sohu-swiper, .app-star-vbox, .app-guess-vbox, .main-rec-view-box, .app-qianfan-box, .comment-empty-bg, .copyinfo, .ph-vbox, .btn_m_action, .btn-xz-app, #film_top_banner, .btn-comment-app",
      css: "\n.comment-empty-txt {\n  margin-bottom: 0;\n}\n\n.app-view-box + footer {\n  padding: 0;\n  opacity: 0.5;\n}\n\n#sohuplayer #menu {\n  z-index: 2147483647;\n}"
    }, {
      name: "乐视·搜索",
      match: /m.le.com\/search|so.le.com\/s/,
      jump: ".column_tit | before, .Relate | before"
    }, {
      name: "乐视·播放页",
      match: /\.le\.com\/(ptv\/vplay\/|vplay_)/,
      vip: ".introduction_box, .briefIntro_left .info_list",
      title: ".briefIntro_info .info_tit, #j-introduction h2",
      fixUrl: !0,
      hide: ".gamePromotion, .gamePromotionTxt, #j-leappMore, .lbzDaoliu, .arkBox"
    }, {
      name: "咪咕.cn·搜索",
      match: /\.migu\.cn\/search\.html/,
      jump: ".pagination, .copyright | before",
      keyword: /content=([^&\?\/\.]+)/,
      hide: ".down-btn"
    }, {
      name: "咪咕·搜索",
      match: /\.miguvideo\.com\/.*search.html/,
      jump: ".search-pagination, .search-main",
      keyword: /keywords=([^&\?\/\.]+)|\?.*#([^&\?\/\.]+)/
    }, {
      name: "咪咕·播放页",
      match: /miguvideo\.com\/.+\/detail\.html/,
      vip: ".playerFooter, .programgroup",
      title: ".left-box .title, .episodeTitle, .video_title",
      hide: '.group-item[name*="广告"], .openClient'
    }, {
      name: "PPTV·搜索",
      match: /sou.pptv.com\/s_video.+kw=|msou.pptv.com\/s_video\/.+keyword=/,
      jump: ".pagination, .zhengpian-box | append"
    }, {
      name: "PPTV·播放页",
      match: /(v|m)\.pptv\.com\/show\//,
      vip: ".m .cf, .vod-tit, .vod-intor",
      title: "#video-info h1, .vod-tit-in span, .tit",
      fixUrl: !0,
      hide: '.w-video-vastad, #video-download-game, div[class*="openapp"], div[class*="side-adv"], div[id*="afp_"], div[id*="-afp"], iframe[src*="/game/"], .afpPosition, .download-iconbar'
    }, {
      name: "华数·搜索",
      match: /wasu\.cn\/.+Search\/.+k=/,
      jump: "#topVod"
    }, {
      name: "华数·播放页",
      match: /wasu\.cn\/.*[pP]lay\/show\//,
      vip: ".movie_title",
      title: ".movie_title h2",
      fixUrl: !0,
      hide: 'div[id*="BAIDU"], .player_menu_con, body > div[style*="fixed"]'
    }, {
      name: "1905·搜索",
      match: /\.1905\.com\/(Search|search)/,
      jump: ".pagination, #new_page"
    }, {
      name: "1905·播放页",
      match: /1905.com\/play/,
      vip: ".playerBox-info, #movie_info, .player-nav",
      title: "#movie_info .infoInner .title, .movie-title, .tv_title",
      fixUrl: !0,
      hide: "#app_store, .openMembershipBtn, body > div[id] > iframe, .pv2-advertisement, .open-app",
      css: "\n#movie_info {\n  margin-top: 1em;\n}"
    }, {
      name: "完美看看·搜索",
      match: /wanmeikk\.me\/search/,
      jump: ".stui-page, .stui-pannel"
    }, {
      name: "完美看看",
      match: /wanmeikk\.me/,
      hide: ".container ~ *[id]"
    }, {
      name: "飞极速·搜索",
      match: "feijisu8.com/search",
      jump: "#result"
    }, {
      name: "飞极速",
      match: /feijisu8\.com/,
      hide: ".index-top ~ div, .v-top ~ div, .footer ~ div, .footer ~ brde, body > div:not([class])"
    }, {
      name: "1090影视·搜索",
      match: /1090ys.com\/.+c=search/,
      jump: ".stui-page, .stui-pannel"
    }, {
      name: "1090影视",
      match: /1090ys\.com/,
      hide: ".container ~ *[id]",
      css: "\nbody {\n  padding-bottom: 0 !important;\n}"
    }, {
      name: "哔滴·搜索",
      match: /bde4.com\/search\//,
      jump: ".search-list"
    }, {
      name: "哔滴",
      match: /bde4\.com/,
      hide: "body > *[id]"
    }, {
      name: "云播·搜索",
      match: "yunbtv.com/vodsearch",
      jump: ".pager",
      keyword: ".breadcrumb font"
    } ], PurifyKeywordRegex = /.*《|》.*|\s*第.{1,3}[季集][\s\d]*$|\s+\d{2,3}\s*$/g, CommonSearchKeywordRegex = /(wd|key|keyword|keyWord|kw|q)=([^&\?\/\.-]+)|(search\/|seacher-|q_)([^&\?\/\.-]+)/, PurifyStyle = "\ndisplay: none !important;\nvisibility: hidden !important;\nwidth: 0 !important;\nheight: 0 !important;\nmax-width: 0 !important;\nmax-height: 0 !important;\noverflow: hidden !important;\nposition: absolute !important;\nleft: -99999px !important;\nopacity: 0 !important;\npointer-events: none !important;\nz-index: -1 !important;", PlayerSelector = "#iframaWrapper, #mgtv-player-wrap, #sohuplayer .x-player, #wPlayer, #video-box, #playerbox, .td-h5__player, .td-playbox, .iqp-player, .g-play .video-area, #mod_player, #playBox, #j-player, #video, .m-video-player, .site_player", PlayerCover = "https://p.pstatp.com/origin/ff460000f53068309d77", VERSION = "20.3.13", svgPlayIcon = '<svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><g><g><circle cx="256" cy="256" fill="#d80027" r="240"/><path d="m208.538 344v-176l145.924 88z" fill="#e0e0e2"/></g><g><g><path d="m431.36 80.64a248 248 0 1 0 -350.72 350.72 248 248 0 1 0 350.72-350.72zm-11.31 339.41a232 232 0 0 1 -328.1-328.1 232 232 0 0 1 328.1 328.1z"/><path d="m176 464.7a7.982 7.982 0 0 1 -2.963-.571 224.077 224.077 0 0 1 -141.037-208.129 8 8 0 0 1 16 0 208.073 208.073 0 0 0 130.965 193.271 8 8 0 0 1 -2.965 15.429z"/><path d="m216.009 476.305a8.072 8.072 0 0 1 -1.482-.138c-5.557-1.041-11.141-2.309-16.595-3.77a8 8 0 1 1 4.136-15.455c5.063 1.355 10.245 2.533 15.405 3.5a8 8 0 0 1 -1.464 15.865z"/></g><path d="m208.538 352a8 8 0 0 1 -8-8v-176a8 8 0 0 1 12.131-6.851l145.924 88a8 8 0 0 1 0 13.7l-145.924 88a8 8 0 0 1 -4.131 1.151zm8-169.833v147.666l122.433-73.833z"/></g></g></svg>', Href = location.href;
    if (!Is(/m\.le\.com/) || !IsNot(/m.le.com\/search|so.le.com\/s|\.le\.com\/(ptv\/vplay\/|vplay_)/)) {
      var D = decodeURIComponent, charToNum = function(n) {
        return n.charCodeAt(0) - 97;
      }, numToChar = function(n) {
        return String.fromCharCode(97 + n);
      }, DEBUG = !1, OO_SIGN = D(atob("KG8lQ0IlOTglRTIlOTclQTElQ0IlOThvKQ")), screenWidth = window.screen.width, isMobile = screenWidth <= 600 || /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || DEBUG && Is(/\/\/m\./), isTop = window.top === window.self, collapseNum = isMobile ? 8 : 12;
      IsNot(/douban\.com/) && isMobile && SEARCH_URLS && (SEARCH_URLS = "选影视  https://movie.douban.com/tag/#/\n\n" + SEARCH_URLS);
      var E = mitt(), logs = [], logColors = {
        success: "Green",
        info: "DodgerBlue",
        warn: "Orange",
        error: "Red"
      };
      log("✔ Loaded", isMobile ? "isMobile" : "isNotMobile");
      var cachePrefix = "oo.";
      await addJs("https://cdn.bootcss.com/jquery/3.4.1/jquery.min.js", {
        name: "jquery",
        version: "3.4.1"
      });
      var $ = jQuery.noConflict(!0);
      SEARCH_URLS = ensureArray(SEARCH_URLS).map((function(n) {
        var o = parseUrl(n);
        return {
          url: o.url,
          name: o.name
        };
      }));
      var maxVipWeight = 1, NoMobileVipUrlRegex = /jx\.wslmf\.com|beijixs\.cn/;
      VIP_URLS = ensureArray(magicUrl(VIP_URLS)).filter((function(n) {
        return !isMobile || !NoMobileVipUrlRegex.test(n);
      }));
      var VIP_ADDON_URLS = VIP_URLS.filter((function(n) {
        return n.includes("✔");
      })), isVipUrlRegex = toUrlRegex(VIP_URLS), isVipAddonUrlRegex = VIP_ADDON_URLS.length > 0 ? toUrlRegex(VIP_ADDON_URLS) : /$^/, vipSiteSign = Href.match(/[\.\/](\w+)\.(\w+)\//);
      vipSiteSign = vipSiteSign ? vipSiteSign[1] : "oo.movie", VIP_URLS = VIP_URLS.map((function(n) {
        var o = parseUrl(n.replace(/=http.+/g, "="));
        if (o.weight = 0, o.url.includes("✔") && (o.url = o.url.replace(/\s*✔.*/g, ""), 
        o.url.startsWith("https") && (o["✔"] = !0)), o.name.includes("￥")) {
          var e = o.name.split(/\s*￥\s*/);
          if (1 === e.length ? e = e[0] : (o.name = e[0].replace(/^\s*[\d\.]+\s*/, ""), e = e[1]), 
          e.includes(vipSiteSign)) {
            var i = e.split(/\s*\|\s*/);
            o.weight += .05 * (i.length + 1), i.forEach((function(n) {
              if (n.includes(vipSiteSign)) {
                isMobile && n.includes(",m") && (o.isM = !0);
                var e = n.match(/,([\d\.]+)/);
                e && (o.weight += Number(e[1]));
              }
            })), o.url.includes("m1907") ? o.weight = o.weight * (isMobile ? .3 : .35) : o.url.includes("yingxiangbao") ? o.weight = .7 * o.weight : o.url.includes("beijixs") && (o.weight = .45 * o.weight);
          }
        } else o.weight = -1;
        return maxVipWeight = Math.max(maxVipWeight, o.weight), o;
      }));
      var getVipUrlWeight = function(n) {
        return n.isM ? n.weight + 100 : n.weight;
      };
      VIP_URLS.sort((function(n, o) {
        return getVipUrlWeight(o) - getVipUrlWeight(n);
      })), maxVipWeight *= isMobile ? 1.2 : 1.1;
      var ooPlayUrl = "", MAX_SNIFF_PLAY_COUNT = 6, playerTitle = "", isHiker = !!window.fy_bridge_app, isMixia = !!window.mx_browser_obj, isAllowHikerSniff = isHiker && !!window.fy_bridge_app.getNetworkRecords, isAllowMixiaSniff = isMixia && !!window.mx_browser_obj.getweblog, isAllowSniff = isAllowHikerSniff || isAllowMixiaSniff, sniffTimestamp = Date.now(), isSniffing = !1, sniffTick = 0, sniffUrls = [], sniffUrlsKey = [];
      log("【嗅探】isAllowSniff:", isAllowSniff);
      var isInvalidSniffUrlRegex = /btrace.video.qq.com|qzonestyle.gtimg.cn|dplayer\/\w+.mp4/, isBadVideoUrlRegex = /(71edge|com-l-youku)\.com/, isValidUrlRegex = /^(http|\/\/)/, hlsUrl = "https://cdn.bootcss.com/hls.js/0.13.2/hls.min.js";
      if (E.on("play.video", (function(n) {
        log("✔ Sniff url: ".concat(n.url, " (").concat(n.from, ")"), "success"), sniffSuccess(n.url, n.from, !1);
      })), DEBUG && isTop) {
        addCss("\noo-logs {\n  position: fixed;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  z-index: 9999;\n  background-color: white;\n  padding: 1em;\n  border: 1px solid #eee;\n  border-radius: 5px;\n  max-height: 200px;\n  overflow-y: scroll;\n}\n\noo-logs:not(.is-active) {\n  display: none;\n}\n\noo-log {\n  display: block;\n  border-bottom: 1px solid #eee;\n  padding: .5em;\n}\n");
        var isLoging = !1;
        $((function() {
          $("html").append("<oo-logs></oo-logs>"), E.on("log.tick", (function() {
            if (!isLoging) {
              for (isLoging = !0; logs.length > 0; ) $("oo-logs").append("<oo-log>".concat(logs.pop(), "</oo-log>"));
              isLoging = !1;
            }
          }));
        }));
      }
      var isAllowIframeSniff = !1, isVideoUrlRegex = /\.(mp4|m3u8|3gp|wmv|flv|avi|rmvb|m4v|ts)/, shortVideoUrlRegex = /\/\/([^\/]+)\/(.*\/)?([^\/]+\.(mp4|m3u8|3gp|wmv|flv|avi|rmvb|m4v|ts))/, messageID = OO_SIGN;
      if (!isTop) return location.href === PlayerCover && window.top.postMessage({
        id: messageID,
        action: "enable.sniff",
        from: location.href
      }, "*"), void $((function() {
        var n = 0, o = setInterval((function() {
          if ($("video source[src], video[src]").length > 0) {
            clearInterval(o);
            var e = $("video source[src]").eq(0).attr("src") || $("video[src]").eq(0).attr("src") || "";
            /^\/[^\/]/.test(e) && (e = location.origin + e), isValidVideoUrl(e) && window.top.postMessage({
              id: messageID,
              action: "play.video",
              from: location.href,
              url: e
            }, "*");
          } else n++ > 100 && clearInterval(o);
        }), 100);
      }));
      if ($((function() {
        $("html").append('<div class="oo-iframes" style="visibility: hidden; position: fixed; bottom: 0; pointer-events: none;"></div>'), 
        addIframe(PlayerCover), window.addEventListener("message", (function(n) {
          var o = n.data;
          o && o.id && o.id === messageID && (log("message:", messageID, o), "enable.sniff" === o.action ? (o.from === PlayerCover && removeIframe(), 
          isAllowIframeSniff || (isAllowIframeSniff = !0, isAllowSniff = !0, $(".oo-vip-list").addClass("has-sniff"))) : o.action && E.emit(o.action, o));
        }));
      })), Is(isVipUrlRegex) || Is(/oo\.movie&/)) Is(/=http/) && ($("title").html(PLUGIN_ID), 
      (Is(isVipAddonUrlRegex) || window.top !== window.self && IsNot(/m1907\.cn/) || Is(/oo\.movie&/)) && vipAddonGo(), 
      $((function() {
        if ($("title").html(PLUGIN_ID), Is(/beijixs\.cn\//)) if (Is(/%\d/)) {
          addCss("\nbody > form {\n  position: absolute !important;\n  left: 0 !important;\n  right: 0 !important;\n  top: 0 !important;\n  bottom: 0 !important;\n  background: #eee8d3 !important;\n  z-index: 2147483647 !important;\n}\n\nform #divcss5 {\n  height: auto;\n  min-height: 90vh;\n}\n\n.oo-fail {\n  height: auto !important;\n}\n\n.oo-fail > div {\n  text-align: center;\n  border-top: 1px solid #495057;\n  border-bottom: 1px solid #495057;\n  padding: 1em;\n  color: #d9480f;\n  font-weight: bold;\n  font-size: 16px;\n  background-color: #fff4e6;\n}\n\n.oo-fail > div > div + div {\n  margin-top: 1em;\n}\n");
          e = 0, i = setInterval((function() {
            var n = document.querySelector("video");
            if (n.duration < 390) {
              n.pause();
              var o = $("#TextBox2").attr("value") || "";
              $(".video-js").addClass("oo-fail").html("<div>\n  <div>解析失败</div>".concat(o ? "<div>" + o + "</div>" : "", "\n</div>")), 
              clearInterval(i);
            }
            e++ > 100 && clearInterval(i);
          }), 100);
        } else {
          var n = Href.split("url=")[1];
          if (n) {
            $("#TextBox1").val(decodeURI(n));
            var o = $("#Button1");
            o.length > 0 && ($(".video-js").css("display", "none !important"), o.click());
          }
          var e = 0, i = setInterval((function() {
            var n = document.querySelector("video");
            n && n.pause(), e++ > 100 && clearInterval(i);
          }), 100);
        } else if (Is(/m1907\.cn/)) {
          addCss("\n#s-player + .show > div[title],\n#s-controls + div > div:nth-child(n+5):not(:last-child)\n{".concat(PurifyStyle, "}\n"));
          e = 0, i = setInterval((function() {
            ($("#s-player + .show").length > 0 || e++ > 30) && (clearInterval(i), $("#s-controls > div img + span").click());
          }), 100);
          window.alert = function() {};
        }
      }))); else {
        if (Is(/m\.tv\.sohu\.com\/phone_play_film.+vid=/)) return location.href = Href.replace("phone_play_film", "v".concat(Href.match(/vid=(\d+)/)[1], ".shtml"));
        if (Is(/search\.douban\.com\/movie\//)) log("豆瓣·电影·搜索 - pc"), addCss("\n#dale_movie_subject_search_bottom,\n#dale_movie_subject_search_top_right,\n#dale_movie_subject_top_right,\n#dale_movie_subject_bottom_super_banner,\n#dale_movie_subject_middle_right {".concat(PurifyStyle, "}\n\n.oo-sources {\n  padding-left: 1em;\n}\n\n.oo-sources a {\n  display: inline-flex !important;\n  align-items: center;\n  border-radius: 4px;\n  font-size: 0.75em;\n  height: 2em;\n  justify-content: center;\n  line-height: 1.5;\n  padding-left: 0.75em;\n  padding-right: 0.75em;\n  white-space: nowrap;\n  background-color: #effaf3;\n  color: #257942;\n  margin-top: 0.5em;\n  margin-right: 0.5em;\n}\n")), 
        $((function() {
          $("#icp").html(OO_SIGN), $(".gemzcp").each((function(n, o) {
            var e = $(".title", o).text();
            $(o).append('<p class="oo-sources">\n'.concat(getSearchSourcesHtml(e), "\n</p>"));
          }));
        })); else if (Is(/m\.douban\.com\/search\/\?.*type=movie/)) log("豆瓣·电影·搜索 - m"), 
        addCss("\n#TalionNav,\n.search-results-modules-name {".concat(PurifyStyle, "}\n\n.search-module {\n  margin-top: 0;\n}\n\n.search-results img {\n  width: 80px;\n}\n\n.search-results {\n  padding-bottom: 10px;\n}\n\n.search-results li a {\n  display: flex;\n  align-items: center;\n}\n\n.search-results img {\n  height: 100%;\n  padding: 0;\n  margin: 5px 0;\n  border: 2px solid;\n  border-image: linear-gradient(to bottom, #2b68c4 0%,#cf2d6e 100%)1;\n}\n")), 
        $((function() {
          $("#more-search").append("    " + OO_SIGN), $(".subject-info").each((function(n, o) {
            insertSearchAddon($(".subject-title", o).text(), o, "append");
          })), $(".search-hd input").on("keyup", (function(n) {
            13 === n.keyCode && (n.preventDefault(), location.href = "/search/?query=" + n.target.value + "&type=movie");
          })), $(".search-hd .button-search").attr("id", OO_SIGN), $(".search-hd .button-search").on("click", (function(n) {
            n.preventDefault();
            var o = $(".search-hd input").val();
            location.href = "/search/?query=" + o + "&type=movie";
          }));
        })); else if (Is(/movie.douban.com\/subject\//)) log("豆瓣·电影·详情 - pc"), addCss("\n#dale_movie_subject_search_bottom,\n#dale_movie_subject_search_top_right,\n#dale_movie_subject_top_right,\n#dale_movie_subject_bottom_super_banner,\n#dale_movie_subject_middle_right {".concat(PurifyStyle, "}\n")), 
        $((function() {
          $("#icp").html(OO_SIGN), log("tick");
          var n = purifyKeyword($("title").text().replace("(豆瓣)", "").trim());
          log("title:", n), log("#info:", $("#info").length), $("#info").append('<div>\n<span class="pl">在线观看:</span>\n<span>\n'.concat(SEARCH_URLS.filter((function(n) {
            return !/douban\.com/.test(n.url);
          })).map((function(o) {
            return "<span><a " + (isMobile ? "" : 'target="_blank" ') + 'href="' + toSearchUrl(o.url, n) + '">' + o.name + "</a>";
          })).join(" / </span>"), "\n</span></span></div>\n"));
        })); else if (Is(/m\.douban\.com\/movie\/subject\//)) log("豆瓣·电影·详情 - m"), addCss("\n.score-write,\na[href*='to_app']:not(.sub-honor):not(.sub-cover),\na[href*='doubanapp'],\ndiv[id*='BAIDU'],\ndiv[id*='google'],\nsection + .center,\n.bottom_ad_download,\n.sub-vendor,\n.to_pc,\n.TalionNav-static,\n.sub-detail .mark-movie,\n.sub-detail .mark-tv,\n.subject-banner,\n.bottom_ad_download,\n.cover-count,\n#google_esf,\n.adsbygoogle,\n.Advertisement,\n.TalionNav-primary .nav-btns.cancel {".concat(PurifyStyle, '}\n\n.sub-info .sub-cover {\n  display: block !important;\n}\n\n.TalionNav-primary {\n  position: relative !important;\n}\n\n.subject-comments,\n.subject-reviews {\n  margin-bottom: 0 !important;\n}\n\n.TalionNav .TalionNav-primary .search-box {\n  width: 220px;\n  flex: 220px 0 0;\n  animation: none;\n}\n\n.sub-original-title {\n  padding: 0.25em 0;\n}\n\n._V_sign {\n  font-size: 0.85em;\n  opacity: 0.15;\n  text-align: center;\n  padding-bottom: 1em;\n}\n\n._V_source, .sub-score + .sub-score {\n  margin-top: 1.5em !important;\n  color: #fff;\n}\n\n._V_source .sub-score .sub-content {\n  display: block;\n}\n\n._V_source .sub-score a {\n  padding: 0.25em 0.5em;\n  line-height: 1.5;\n  margin: 0 0.15em;\n  border: 1px solid rgba(255,255,255,0.2);\n  font-size: 1.05em;\n  font-weight: bold;\n  letter-spacing: 1px;\n  margin-top: 0.5em;\n  display: inline-block;\n  color: #ffe8cc;\n  background: rgba(239, 238, 238, 0.05);\n  border-radius: 4px;\n}\n\n#TalionNav {\n  display: none;\n}\n\n#TalionNav .logo {\n  background: none;\n  font-size: 1em;\n  display: inline-flex;\n  justify-content: center;\n  align-items: center;\n  color: #dee2e6;\n}\n\n.search-box:not(.on-search) {\n  opacity: 0.7;\n}\n\n#channel_tags {\n  margin-bottom: 10px;\n}\n\n.subject-header-wrap .sub-detail {\n  display: flex;\n  flex-direction: column;\n  justify-content: space-around;\n}\n\n#channel_tags {\n  margin-top: 10px;\n}\n\ninput[type="search"]::-webkit-search-decoration,\ninput[type="search"]::-webkit-search-cancel-button,\ninput[type="search"]::-webkit-search-results-button,\ninput[type="search"]::-webkit-search-results-decoration {\n  -webkit-appearance:none;\n}\n')), 
        $((function() {
          $(".movie-reviews .show-all").after('<div class="_V_sign"><a href="https://gitee.com/ecruos/oo">豆瓣·净化 '.concat(OO_SIGN, "</a></div>")), 
          $("section + .center").each((function(n, o) {
            $(o).remove();
          })), $(".subject-header-wrap").after($("#TalionNav")), $("#TalionNav").css("display", "block"), 
          $("#TalionNav .logo").html(OO_SIGN).attr("href", "https://movie.douban.com/tag/#/"), 
          $(".search-box").remove(), $(".TalionNav-primary .logo").after('<div class="search-box"><input class="search-input" type="search" placeholder="搜索"></div>'), 
          $(".search-input").on("focus", (function() {
            $(this).parent().addClass("on-search");
          })).on("blur", (function() {
            $(this).parent().removeClass("on-search");
          })), $(".search-input").on("keyup", (function(n) {
            13 === n.keyCode && (n.preventDefault(), location.href = "/search/?query=" + n.target.value + "&type=movie");
          }));
          var n = purifyKeyword($(".sub-title").text().trim());
          0 === $("._V_source").length && $(".subject-header-wrap").append('<div class="_V_source subject-mark">\n\n<div class="sub-score">\n  <div class="sub-trademark">\n  在线观看\n  </div>\n  <div class="sub-content">\n'.concat(getSearchSourcesHtml(n, !1), "\n  </div>\n</div>\n\n</div>")), 
          setTimeout((function() {
            $(".subject-intro .bd p").click(), $(".sub-cover").attr("href", "#"), $("#subject-honor-root a").attr("href", "#");
          }), 1e3);
          var o = 0, e = setInterval((function() {
            $("body > ins, body > iframe, .adsbygoogle").remove(), o++ > 5 && clearInterval(e);
          }), 500);
          !function n() {
            var o = $("#subject-header-container").attr("style");
            if (o) {
              var e = o.match(/:\s*([^;]+);?/)[1], i = e.replace(")", ", 0)");
              try {
                addCss("\n.sub-cover::before {\n  background: -webkit-linear-gradient(bottom, ".concat(e, " 0%, ").concat(i, " 15%), -webkit-linear-gradient(right, ").concat(e, " 0%, ").concat(i, " 15%),-webkit-linear-gradient(top, ").concat(e, " 0%, ").concat(i, " 15%), -webkit-linear-gradient(left, ").concat(e, " 0%, ").concat(i, ' 15%);\n  content: "";\n  bottom: 0;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 0;\n  width: 102px;\n  height: 142px;\n  border-radius: 4px;\n}\n'));
              } catch (n) {
                console.error("syncCoverColor:", n);
              }
            } else setTimeout((function() {
              n();
            }), 100);
          }();
        })); else if (isMobile && Is(/movie\.douban\.com\/tag\/#/)) {
          log("豆瓣·选影视");
          var num = 3;
          addCss(prefixCss("\n.category {\n  width: 100%;\n  white-space: nowrap;\n  overflow-x: auto;\n}\n\n.tags {\n  margin-bottom: 1em !important;\n}\n\n.checkbox__input {\n  vertical-align: text-top;\n}\n\n.tag-nav {\n  margin: 0 auto;\n  font-size: 12px;\n  width: 100%;\n}\n\n.tag-nav .tabs, .tag-nav .check {\n  display: flex;\n  justify-content: space-around;\n  width: 100%;\n}\n\n.tag-nav .tabs a {\n  padding: 7.5px 5px 5px;\n}\n\n.tabs a:not(.tab-checked) {\n  border: 1px solid #dfdfdf;\n}\n\n.tabs .tab-checked {\n  border: 1px solid #258dcd!important;\n}\n\n.tab-checked:after {\n  display: none;\n}\n\n.checkbox, .range {\n  margin-right: 5px;\n}\n\n.check {\n  float: none;\n  margin-top: 5px;\n}\n\n.list-wp, .item .cover-wp {\n  overflow: unset;\n}\n\na img {\n  padding: 2px;\n  border-radius: 5px;\n  background: linear-gradient(to bottom, #2b68c4 0%,#cf2d6e 100%);\n}\n\na.item {\n  width: ".concat(parseInt(100 / num), "%;\n  text-align: center;\n}\n\na.item p {\n  padding-right: 0;\n}\n\na.item .cover-wp {\n  height: auto;\n  padding: 0 0.5em;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\na.item .cover-wp:after, .poster:after {\n  display: none;\n}\n\na.item .pic img {\n  width: 100%;\n  height: ").concat(parseInt(4 * screenWidth / 3 / num), "px;\n  max-width: 150px;\n  object-fit: cover;\n}\n\n.tag-nav .range-dropdown {\n  left: 0 !important;\n  width: auto !important;\n  right: 0 !important;\n  top: -4em !important;\n}\n\n.more {\n  margin: 0 1em 0.5em !important;\n}\n\n"), ".oo") + "\nbody > *:not(.oo) {".concat(PurifyStyle, "}\n\n#app .article, .article.oo {\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  padding: 10px 6px;\n  transition: all 0.8s;\n}\n\n.category::-webkit-scrollbar {\n  width: 1px;\n  height: 1px;\n  background-color: rgba(223, 223, 223, 0.25);\n}\n\n.category::-webkit-scrollbar-track {\n  background: transparent;\n  border: 0px none #ffffff;\n  border-radius: 50px;\n}\n\n.category::-webkit-scrollbar-thumb {\n  -webkit-box-shadow: inset 0 0 2.5px rgba(0, 0, 0, 0.1);\n  box-shadow: inset 0 0 2.5px rgba(0, 0, 0, 0.1);\n  border-radius: 2.5px;\n  background-color: rgba(223, 223, 223, 0.25);\n  opacity: 0.7;\n  transition: opacity ease-in-out 200ms;\n}\n\n.category::-webkit-scrollbar-thumb:hover {\n  opacity: 1;\n  background-color: rgba(223, 223, 223, 0.25);\n}\n\n.oo-search {\n  position: relative;\n  display: flex;\n  margin-bottom: 5px;\n}\n\n.oo-search .inp {\n  height: 34px;\n  text-align: center;\n  cursor: text;\n  width: 90%;\n  border-top: 1px solid #dedede;\n  border-left: 1px solid #dedede;\n  border-top-left-radius: 3px;\n  border-bottom-left-radius: 5px;\n}\n\n.oo-search .inp input {\n  background: #fff;\n  width: 96%;\n  margin: 0;\n  text-align: left;\n  height: 30px;\n  padding-left: 10px;\n  outline: none;\n}\n\n.oo-search input {\n  -webkit-appearance: none;\n  border: none;\n  background: transparent;\n}\n\n.oo-search .inp-btn {\n  position: relative;\n  width: 37px;\n  height: 34px;\n}\n\n.oo-search .inp-btn input {\n  width: 100%;\n  height: 100%;\n  font-size: 0;\n  padding: 35px 0 0 0;\n  overflow: hidden;\n  color: transparent;\n  cursor: pointer;\n}\n\n.oo-search .inp-btn input:focus {\n  outline: none;\n}\n\n.oo-search .inp {\n  background-image: url(//img3.doubanio.com/dae/accounts/resources/a4a38a5/movie/assets/nav_mv_bg.png?s=1);\n}\n\n.oo-search .inp-btn input {\n  background: url(//img3.doubanio.com/dae/accounts/resources/a4a38a5/movie/assets/nav_mv_bg.png?s=1) no-repeat 0 -40px;\n}\n\n.oo-item-balancer {\n  margin-left: 11%;\n  margin-right: 11%;\n}\n")), 
          $((function() {
            $("title").html("选影视 - oo.movie"), $("#app .article .tags").before('<div class="oo-search">\n  <div class="inp"><input name="'.concat(OO_SIGN, '" size="22" maxlength="60" placeholder="搜索电影、电视剧、综艺、影人" value="" autocomplete="off"></div>\n  <div class="inp-btn"><input type="submit" value="搜索"></div>\n</div>')), 
            $("body").prepend($("#app .article").addClass("oo")), $(".oo-search input").on("keyup", (function(n) {
              13 === n.keyCode && (n.preventDefault(), location.href = "https://m.douban.com/search/?query=" + n.target.value + "&type=movie");
            })), $(".oo-search .inp-btn input").on("click", (function(n) {
              n.preventDefault();
              var o = $(".oo-search input").val();
              location.href = "https://m.douban.com/search/?query=" + o + "&type=movie";
            })), $("a.item").each((function(n, o) {
              $(o).attr("href", $(o).attr("href").replace("movie.douban.com", "m.douban.com/movie")).removeAttr("target");
            }));
            var n = document.querySelector(".list-wp");
            new MutationObserver((function(n) {
              for (var o of n) "childList" == o.type && (isChildChanged = !0, o.addedNodes.forEach((function(n) {
                n.classList.contains("item") && (n.setAttribute("href", n.getAttribute("href").replace("movie.douban.com", "m.douban.com/movie")), 
                n.removeAttribute("target"));
              })));
              $(".list-wp .item").eq(-2).addClass("oo-item-balancer");
            })).observe(n, {
              attributes: !0,
              childList: !0
            });
          }));
        } else Is(/\w+:1234|ecruos\.gitee\.io\/one/) ? (log("One·主页"), $((function() {
          localStorage.setItem("One.plugin.version", VERSION);
        }))) : Is(/\.bilibili\.com\/bangumi\/play\//) && (log("哔哩哔哩·影视播放页"), $((function() {
          var n = 0, o = setInterval((function() {
            if ($(".ep-info-image img, .media-cover img").length > 0) {
              var e = $(".media-title, .ep-info-title").eq(0).text();
              e && (clearInterval(o), insertSearchAddon(e, ".media-wrapper, .ep-list-pre-wrapper"));
            }
            n++ > 100 && clearInterval(o);
          }), 100);
        })));
      }
      makeBetterAddons(), callbackWhenDone(), Is(/m\.v\.qq\.com/) && urlDetector((function() {
        if (Is(/v\.qq\.com\/(cover|play|x\/cover|x\/page|x\/play|x\/m\/cover|x\/m\/page|x\/m\/play)/)) var n = 0, o = !1, e = setInterval((function() {
          o || (o = !0, makeBetterAddons(), n++ > 100 || $(".oo-vip").length > 0 ? clearInterval(e) : o = !1);
        }), 100);
      })), $((function() {
        $("#a1, .dplayer").length > 0 && addCss("\nbody:after,\n#a1 ~ script ~ div,\n.dplayer > *[id^=ad]\n{".concat(PurifyStyle, "}\n"));
      }));
    }
  }
  function Is(n) {
    return n.test(Href);
  }
  function IsNot(n) {
    return !Is(n);
  }
  function Reverse(n) {
    let o = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "";
    return n.split("").reverse().join(o);
  }
  function RReverse(n) {
    let o = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "";
    return n.split(o).reverse().join("");
  }
  function randomInt(n, o) {
    return Math.floor(Math.random() * (o - n + 1) + n);
  }
  function magicNumber(n) {
    return Number(n) + (n % 2 == 1 ? -1 : 1);
  }
  function magicLetter(n) {
    return numToChar(magicNumber(charToNum(n)));
  }
  function magicChar(n) {
    return /[a-z]/.test(n) ? magicLetter(n) : /\d/.test(n) ? magicNumber(n) : "";
  }
  function magicString(n) {
    return n.split("").reverse().map((function(n) {
      return magicChar(n);
    })).join("");
  }
  function magicUrl(n) {
    return n.replace(/([\/\.])(\w+)(\.\w+\/.+✔)/g, (function(n, o, e, i) {
      return o + magicString(e) + i;
    }));
  }
  function mitt(n) {
    return n = n || Object.create(null), {
      on(o, e) {
        (n[o] || (n[o] = [])).push(e);
      },
      off(o, e) {
        n[o] && n[o].splice(n[o].indexOf(e) >>> 0, 1);
      },
      emit(o, e) {
        (n[o] || []).slice().map((function(n) {
          n(e);
        })), (n["*"] || []).slice().map((function(n) {
          n(o, e);
        }));
      }
    };
  }
  function log() {
    if (DEBUG) {
      for (var n = "black", o = arguments.length, e = new Array(o), i = 0; i < o; i++) e[i] = arguments[i];
      var t = [ "[".concat((new Date).toISOString().replace(/.+T|\..+/g, ""), "] ").concat(PLUGIN_ID, " →"), ...e ], a = logColors[t[t.length - 1]];
      a && (n = a, t.pop());
      var r = t.map((function(n) {
        return "object" == typeof n ? JSON.stringify(n) : n;
      })).join(" ");
      logs.unshift(r), E.emit("log.tick"), console.log("%c" + r, "color:" + n);
    }
  }
  function getKeywordFromUrl(n, o) {
    var e = (o || location.href).match(n || CommonSearchKeywordRegex);
    return e ? D((n ? e[1] || e[2] : e[2] || e[4]) || "") : "";
  }
  function purifyKeyword(n) {
    return (PurifyKeywordRegex ? n.replace(PurifyKeywordRegex, "") : n).replace(/\s*:\s*$/, "").trim();
  }
  function prefixCss(n, o) {
    var e, i, t, a, r = o.length;
    o += " ", n = (n = (n = n.replace(/\/\*(?:(?!\*\/)[\s\S])*\*\/|[\r\n\t]+/g, "")).replace(/}(\s*)@/g, "}@")).replace(/}(\s*)}/g, "}}");
    for (var s = 0; s < n.length - 2; s++) e = n[s], i = n[s + 1], "@" === e && (t = !0), 
    t || "{" !== e || (a = !0), a && "}" === e && (a = !1), a || "@" === i || "}" === i || "}" !== e && "," !== e && ("{" !== e && ";" !== e || !t) || (n = n.slice(0, s + 1) + o + n.slice(s + 1), 
    s += r, t = !1);
    return 0 !== n.indexOf(o) && 0 !== n.indexOf("@") && (n = o + n), n;
  }
  function addCss(n, o) {
    let e;
    /^http/.test(n) ? (e = document.createElement("link"), e.rel = "stylesheet", e.href = n) : (o && (n = prefixCss(n, o)), 
    n = n.replace(/\n+\s*/g, " "), e = document.createElement("style"), e.styleSheet ? e.styleSheet.cssText = n : e.appendChild(document.createTextNode(n))), 
    e.type = "text/css", document.getElementsByTagName("head")[0].appendChild(e);
  }
  function getCache(n) {
    try {
      var o = JSON.parse(window.localStorage.getItem(cachePrefix + n));
      return o.data ? o : null;
    } catch (n) {
      return null;
    }
  }
  function setCache(n, o) {
    let e = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : VERSION;
    o && window.localStorage.setItem(cachePrefix + n, JSON.stringify({
      data: o,
      version: e
    }));
  }
  async function addJs(n) {
    let o = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
    var e, {name: i = n, version: t = VERSION} = o, a = getCache(i);
    e = a && a.version === t ? a.data : await window.fetch(n).then((function(n) {
      return n.text();
    })).then((function(n) {
      return setCache(i, n, t), n;
    }));
    var r = document.createElement("script");
    r.innerHTML = e, r.oo = "movie", document.getElementsByTagName("head").item(0).appendChild(r);
  }
  function $$(n, o) {
    let e = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 0;
    0 !== $(n).length ? (log("✔ waitToRun:", n), $((function() {
      o();
    }))) : e > 100 ? log("× waitToRun:", n, "warn") : setTimeout((function() {
      waitToRun(n, o, e + 1);
    }), 100);
  }
  function uuid() {
    let n = 46656 * Math.random() | 0, o = 46656 * Math.random() | 0;
    return n = ("000" + n.toString(36)).slice(-2), o = ("000" + o.toString(36)).slice(-3), 
    "o" + n + o;
  }
  function fixUrl(n) {
    return n.replace(/[\?#].+/g, "");
  }
  function fixVipUrl(n) {
    var o = BETTER_ADDONS.find((function(o) {
      return o.fixUrl && o.match.test(n);
    }));
    return o ? !0 === o.fixUrl ? fixUrl(n) : o.fixUrl(n) : n;
  }
  function parseUrl(n, o) {
    var e, i, t = n.trim().split(/[\s@]+/), a = t.filter((function(n) {
      return /https?:\/\//.test(n);
    }));
    t = t.filter((function(n) {
      return !/https?:\/\//.test(n);
    })), a.forEach((function(n) {
      /\/\/m\.|\/m\/|\/\/msou/.test(n) ? e = n : i = n;
    }));
    var r = (isMobile ? e : i) || a[0];
    o && (r = toSearchUrl(r, o));
    var s = t.length > 0 ? t.join(" ") : r.match(/\/\/(.+\.)?([^\/]+)\.\w+\//)[2].replace(/^(\w)/, (function(n) {
      return n.toUpperCase();
    }));
    return {
      url: r,
      name: s
    };
  }
  function toSearchUrl(n, o) {
    return n.includes("**") ? n.replace("**", o) : /movie\.douban\.com\/tag/.test(n) ? n : n + o;
  }
  function ensureArray(n) {
    return Array.isArray(n) ? n : n.trim().split(/[\n\s]*\n+[\n\s]*/);
  }
  function toUrlRegex(n) {
    return new RegExp(n.map((function(n) {
      return n.replace(/.+\/\/|\/.+/g, "").replace(/\./g, "\\.");
    })).join("|"));
  }
  function pausePlay() {
    try {
      $("video, audio").each((function(n, o) {
        o.pause(), o.muted = "muted", $(o).remove();
      }));
    } catch (n) {
      console.error(PLUGIN_NAME + " play: " + n);
    }
  }
  function smartCollapse(n) {
    log("smartCollapse:", n);
    var o = 0, e = !1;
    $(n).each((function(n, i) {
      n < collapseNum ? n >= collapseNum - 1 && (o = $(i).position().top) : e || o !== $(i).position().top ? (e = !0, 
      $(i).prev("").addClass("oo-hide")) : !e && $(i).hasClass("oo-collapse") && $(i).addClass("oo-hide");
    }));
  }
  function getVipTargetUrl() {
    if (ooPlayUrl) return ooPlayUrl;
    var n = Is(isVipUrlRegex) ? location.href.replace(/.+=http/, "http") : location.href.replace(/&?\w+=http[^&]+/, "").replace(/.+http/, "http");
    return n = decodeURI(fixVipUrl(n) || n);
  }
  function isValidVideoUrl(n) {
    return isValidUrlRegex.test(n) && isVideoUrlRegex.test(n) && !isInvalidSniffUrlRegex.test(n) && (!isBadVideoUrlRegex.test(n) || Date.now() - sniffTimestamp > 8e3);
  }
  function fixVideoUrl(n) {
    return n.replace(/http:\/\/(vwecam.tc.qq.com)/, "https://$1");
  }
  async function playBefore() {
    await addJs(hlsUrl, {
      name: "hls",
      version: "0.13.2"
    });
  }
  function sniffStart(n) {
    isSniffing = !0, sniffTimestamp = Date.now(), sniffUrls = [], sniffUrlsKey = [], 
    n && ($(".oo-vip-play").addClass("is-hide").empty(), $(".oo-vip-list").addClass("is-sniffing"), 
    $(".oo-notification").remove());
  }
  function sniffDone() {
    isSniffing = !1, $(".oo-vip-list").removeClass("is-sniffing");
  }
  function sniffSuccess(n, o) {
    let e = arguments.length > 2 && void 0 !== arguments[2] && arguments[2];
    var i = n.replace(/\?.+/, "");
    if (sniffUrlsKey.includes(i)) log("Skip duplicate sniff url:", n); else {
      sniffUrls.push(n), sniffUrlsKey.push(i), log("✔ sniffSuccess:", n);
      var t = $(".oo-vip-play").length > 0;
      t || $(".oo-vip-list").before('<div class="oo-vip-play"><div class="oo-sources"></div><div class="oo-play-url"></div></div>');
      var a = DEBUG ? ' _from="'.concat(o, '"') : "", r = n.match(isVideoUrlRegex)[1];
      $(".oo-vip-play .oo-sources").append('<a _href="'.concat(n, '"').concat(a, ">线路").concat(sniffUrls.length, '<span class="oo-badge">').concat(r, "</span></a>")), 
      $(".oo-vip-play .oo-sources a").off("click").on("click", (function() {
        var n = fixVideoUrl($(this).attr("_href"));
        log("playing:", n, "success");
        var o = document.querySelector(".oo-player-box video");
        o || ($(".oo-player-box").html('<video class="oo-video" _src="'.concat(n, '" poster="').concat(PlayerCover, '" controls="true" playsinline="true" preload="auto"></video>')), 
        o = document.querySelector(".oo-player-box video"));
        var e = e || HLS;
        if (/\.m3u8/.test(n) && e && e.isSupported()) {
          var i = new e;
          i.loadSource(n), i.attachMedia(o), i.on(e.Events.MANIFEST_PARSED, (function() {
            o.play();
          }));
        } else o.src = n, o.addEventListener("loadedmetadata", (function() {
          o.play();
        }));
        $(".oo-play-url").html('来源：<a _href="'.concat(n, '"><span class="oo-icon">').concat(svgPlayIcon, "</span>").concat(toShortVideoUrl(n), "</a>")), 
        $(".oo-play-url a").off("click").on("click", (function() {
          var n = $(this).attr("_href");
          log("click play url:", n), isHiker && window.fy_bridge_app.playVideo ? window.fy_bridge_app.playVideo(n) : isMixia && window.mx_browser_obj.playvideo ? window.mx_browser_obj.playvideo(n, n) : window.open(n, "_blank");
        })), $(".oo-vip-play .oo-sources a").removeClass("is-active"), $(this).addClass("is-active");
      })), 1 === sniffUrls.length && $(".oo-vip-play a").eq(0).click(), e && (sniffDone(), 
      DEBUG && (emptyIframes(), $(".oo-notification").remove(), $(".oo-vip-list").after('<div class="oo-notification oo-success">解析成功，嗅探到播放地址：<br><code>'.concat(n, "</code></div>"))));
    }
  }
  function sniffFail(n) {
    log("× sniffFail"), sniffDone(), $(".oo-notification").remove(), $(".oo-vip-list").after('<div class="oo-notification'.concat(sniffUrls.length > 0 ? "" : " oo-warning", '">解析结束，如果没有解析成功，').concat(n ? "" : "可以尝试其它解析源，也", "可以考虑在其它正版网站上解析，或使用搜索源搜索在线观看</div>"));
  }
  function sniffLog(n) {
    0 === $(".oo-vip .oo-logs").length && $(".oo-vip").append('<div class="oo-logs"></div>'), 
    $(".oo-vip .oo-logs").html(n);
  }
  function iframeSniff() {
    log("iframeSniff");
  }
  function hikerSniff(sniffTickId) {
    if (sniffTickId === sniffTick) {
      log("hikerSniff");
      var resource = eval(window.fy_bridge_app.getNetworkRecords());
      if (resource = resource.filter((function(n) {
        return n.timestamp > sniffTimestamp && /video/i.test(n.mediaType.name) && isValidVideoUrl(n.url);
      })), DEBUG) {
        var logInfo = "Count: ".concat(resource.length, " (").concat((new Date).toISOString(), ")<br>") + resource.map((function(n, o) {
          return "".concat(o + 1, " - ").concat(n.mediaType.name, "(").concat(n.mediaType.type, ") - ").concat(n.url);
        })).join("<br>");
        sniffLog(logInfo);
      }
      isSniffing && (resource.forEach((function(n) {
        sniffSuccess(n.url, "", !1);
      })), setTimeout((function() {
        hikerSniff(sniffTickId);
      }), 100));
    }
  }
  function mixiaSniff(n) {
    if (n === sniffTick) {
      log("mixiaSniff");
      var o = [], e = window.mx_browser_obj.getweblogs("http");
      if ("error" !== e && (o = (o = e.trim().split(/\s*\n[\n\s]*/)).filter((function(n) {
        return isValidVideoUrl(n);
      }))), DEBUG) sniffLog("Count: ".concat(o.length, " (").concat((new Date).toISOString(), ")<br>") + o.map((function(n, o) {
        return "".concat(o + 1, " - ").concat(n);
      })).join("<br>"));
      isSniffing && (o.forEach((function(n) {
        sniffSuccess(n, "", !1);
      })), setTimeout((function() {
        mixiaSniff(n);
      }), 100));
    }
  }
  function sniff(n) {
    let o = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
    if (log("sniff: ".concat(playerTitle, "（isAllowIframeSniff ").concat(isAllowIframeSniff ? "✔" : "×", "）")), 
    sniffStart(o), isAllowIframeSniff) iframeSniff(); else if (isAllowHikerSniff) hikerSniff(n); else {
      if (!isAllowMixiaSniff) return void sniffDone();
      mixiaSniff(n);
    }
    o ? sniffVip() : setTimeout((function() {
      n === sniffTick && sniffFail(o);
    }), 13e3);
  }
  function sniffVip() {
    var n = getVipPlayer();
    n.length > 0 && (n.empty().append('<div class="oo-player-box"><div class="oo-player-bg"></div></div>'), 
    $(".oo-player-box").parent().addClass("oo-player")), pausePlay(), sniffUrls = [], 
    goSniff();
  }
  function goSniff() {
    let n = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 0;
    if (isSniffing) {
      var o = VIP_URLS[n];
      if (log("【".concat(n, "】goSniff:"), o), o) if (sniffUrls.length >= MAX_SNIFF_PLAY_COUNT) sniffDone(); else if (o["✔"]) {
        var e = getVipTargetUrl(), i = o.url + e;
        log("sniff: " + i);
        var t = addIframe(i);
        t.onload = function() {
          setTimeout((function() {
            log("remove iframe"), removeIframe(t), n >= VIP_URLS.length - 1 && sniffFail(!0);
          }), 7e3);
        };
        var a = isMobile ? Math.min(300 * sniffUrls.length + 500, 3500) : Math.min(300 * sniffUrls.length + 2e3, 5e3);
        setTimeout((function() {
          goSniff(n + 1);
        }), a);
      } else goSniff(n + 1); else sniffFail(!0);
    }
  }
  function getVipPlayer() {
    var n = $(PlayerSelector).eq(0);
    return 0 === n.length && (n = $("#player, .player").eq(0)), n;
  }
  function insertVipSource(n) {
    let o = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "after", e = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 0;
    if (!($(".oo-vip").length > 0 || e > 20)) {
      var i = $(n);
      if (0 !== i.length) {
        log("insertVipSource"), addCss("\n.oo-vip {\n  padding-bottom: 0.5em;\n  background-color: rgba(255, 255, 255, 0.05);\n  border-radius: 5px;\n  width: 100%;\n  overflow: hidden;\n  z-index: 999999;\n}\n\n.oo-vip + .oo-vip {".concat(PurifyStyle, "}\n\n.oo-iframes {\n  visibility: hidden;\n  pointer-events: none;\n  position: fixed;\n  bottom: 0;\n  left: -9999px;\n  max-width: 100px;\n  max-height: 50px;\n}\n\n.oo-iframes > iframe {\n  max-width: 20%;\n  display: inline-block;\n}\n\n.oo-vip-panel {\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  padding: 10px 10px 0;\n  font-size: 15px;\n  width: 100%;\n}\n\n.oo-vip-panel.is-setting {\n  padding-bottom: 0.5em;\n  border-bottom: 1px solid rgba(73, 80, 87, 0.15);\n  border-top: 1px solid rgba(73, 80, 87, 0.15);\n  font-size: 0.95em;\n  opacity: 0.8;\n}\n\n.oo-vip-panel.is-setting a {\n  color: #1c7ed6;\n  text-decoration: underline;\n  font-weight: bold;\n  margin-right: 1em;\n}\n\n.oo-vip-panel.is-setting .oo-nav-links {\n  display: flex;\n  justify-content: space-around;\n  width: 100%;\n}\n\n.oo-vip-panel.is-setting:not(.is-open) {\n  display: none;\n}\n\n.oo-vip-panel.is-setting ~ div {\n  border-top: 1px solid rgba(73, 80, 87, 0.15);\n}\n\n.oo-vip-title {\n  font-weight: bold;\n  color: #257942;\n  width: 100%;\n}\n\n.oo-vip-small {\n  font-size: 0.75em;\n  margin: 0 10px;\n  color: #ced4da;\n}\n\n.oo-vip-title, .oo-vip-sign {\n  padding: 0.5em;\n}\n\n.oo-vip-title-text, .oo-vip-sign {\n  cursor: pointer;\n}\n\n.oo-vip-panel, .oo-vip-list {\n  height: auto !important;\n}\n\n.oo-vip-sign {\n  opacity: 0.25;\n  margin-right: 1em;\n  min-width: 5em;\n  text-align: right;\n  animation: oo-color-change-opacity 5s normal infinite ease-in-out;\n}\n\n@keyframes oo-color-change-opacity {\n  0%   { opacity: 0.25; }\n  50% { opacity: 0.45; }\n  100% { opacity: 0.25; }\n}\n\n.oo-vip-list {\n  padding: 0.5em;\n  letter-spacing: 1px;\n}\n\n.oo-vip-list .oo-vip-item {\n  border-radius: 4px;\n  display: inline-flex;\n  justify-content: center;\n  align-items: center;\n  white-space: nowrap;\n  background-color: #eef6fc;\n  color: #1d72aa;\n  margin: 4px;\n  padding: 0.5em 0.5em 0.35em;\n  cursor: pointer;\n  font-size: 14px;\n  line-height: 1.2;\n  font-weight: 600;\n  text-decoration: none;\n  position: relative;\n  overflow: hidden;\n  transition: all 0.25s;\n}\n\n.oo-vip-list .oo-vip-item.hide {\n  display: none;\n}\n\n.oo-vip-list.is-sniffing .oo-vip-item {\n  cursor: not-allowed;\n  opacity: 0.5 !important;\n}\n\n.oo-vip-list.is-sniffing .oo-sniff {\n  animation: oo-beat 0.25s infinite alternate;\n}\n\n.oo-vip-list .oo-vip-item.oo-sniff {\n  color: #099268;\n}\n\n.oo-vip-list.is-sniffing .oo-sniff .oo-vip-weight {\n  background-color: #099268;\n}\n\n.oo-notification {\n  border-radius: 4px;\n  padding: 1em 2em 1em 1em;\n  margin: 0.5em;\n  font-weight: bold;\n  font-size: 0.9em;\n}\n\n.oo-notification.oo-warning {\n  background-color: #ffdd57;\n  color: rgba(0,0,0,0.7);\n}\n\n.oo-notification.oo-success {\n  background-color: #48c774;\n  color: #fff;\n}\n\n@keyframes oo-beat{\n\tto { transform: scale(1.4); }\n}\n\n.oo-collapse {\n  min-width: 2em;\n}\n\n.oo-vip-weight-bg, .oo-vip-weight {\n  position: absolute;\n  bottom: 0;\n  height: 2px;\n  left: -1px;\n  right: -1px;\n  transition: all 0.5s;\n}\n\n.oo-vip-weight-bg {\n  position: absolute;\n  right: -1px;\n  background-color: #ced4da;\n}\n\n.oo-vip-weight-bg.is-full {\n  background-color: #0ca678;\n}\n\n.oo-vip-weight {\n  background-color: #2b8a3e;\n  border-radius: 1px;\n}\n\n.oo-vip-list .oo-vip-item:hover .oo-vip-weight-bg, .oo-vip-list .oo-vip-item:hover .oo-vip-weight, .oo-vip-list .oo-vip-item.is-active .oo-vip-weight-bg, .oo-vip-list .oo-vip-item.is-active .oo-vip-weight {\n  opacity: 0;\n}\n\n.oo-vip-list .oo-vip-item:hover, .oo-vip-list .oo-vip-item:active {\n  background-color: #1d72aa;\n  color: #eef6fc !important;\n}\n\n.oo-vip-list .oo-vip-item.is-good {\n  color: rgb(14, 95, 149);\n}\n\n.oo-vip-list .oo-vip-item.is-bad {\n  opacity: 0.85;\n}\n\n.oo-vip-list .oo-vip-item.is-bad:hover, .oo-vip-list .oo-vip-item.is-bad:active {\n  opacity: 0.95;\n}\n\n.oo-vip-list .oo-vip-item.is-active {\n  background-color: #2b8a3e;\n  color: #eef6fc;\n}\n\n.oo-vip-list.is-open .oo-collapse,\n.oo-sources.is-open .oo-collapse {\n  transform:scaleX(-1);\n}\n\n.oo-vip-play.is-hide {\n  display: none !important;\n}\n\n.oo-vip-play a {\n  position: relative;\n  margin-right: 8px;\n}\n\n.oo-vip-play .oo-badge {\n  position: absolute;\n  top: -.75em;\n  left: -.5em;\n  background-color: #effaf3;\n  padding: .1em .25em .25em;\n  font-size: .8em;\n  transform: rotate(-25deg);\n  color: #f76707;\n  border-top-left-radius: 3px;\n  border-top-right-radius: 3px;\n  font-family: cursive, monospace, sans-serif;\n}\n\n.oo-vip-play a:hover .oo-badge,\n.oo-vip-play a.is-active .oo-badge {\n  color: #effaf3;\n  background-color: #367942;\n}\n\n.oo-play-url {\n  color: #546E7A;\n  padding: .5em 1em;\n}\n\n.oo-play-url a {\n  color: #e67700 !important;\n  text-decoration: underline;\n  cursor: pointer;\n}\n\n.oo-play-url a:hover,\n.oo-play-url a:hover .oo-divider {\n  color: #4CAF50 !important;\n}\n\n.oo-play-url .oo-icon {\n  display: inline-block;\n  width: 1em;\n  padding: 0 .25em;\n  vertical-align: text-top;\n}\n\n.oo-play-url .oo-divider {\n  margin: 0 .25em;\n  color: #f8b248;\n  font-weight: bold;\n}\n\n.oo-vip-list:not(.is-open) .oo-hide {\n  display: none !important;\n}\n\n.oo-vip-tip {\n  border-top: 1px solid rgba(73, 80, 87, 0.15);\n  padding: 1em 1em 0.5em;\n  font-size: 0.85em;\n  line-height: 1.25;\n  color: #343a40;\n  opacity: 1;\n  animation: oo-color-change-light 5s normal forwards ease-in-out;\n}\n\n.oo-vip-tip.is-warning {\n  color: #e8590c;\n  animation: oo-color-change-light2 5s normal forwards ease-in-out;\n}\n\n@keyframes oo-color-change-light {\n  0%   { color: #343a40; opacity: 1; }\n  100% { color: #979aab; opacity: 0.85; }\n}\n\n@keyframes oo-color-change-light2 {\n  0%   { color: #e8590c; opacity: 1; }\n  100% { color: #fd7e14; opacity: 0.85; }\n}\n\n.oo-vip-tip strong {\n  margin: 0 0.5em;\n  font-size: 1.25em;\n  opacity: 0.8;\n}\n\n.oo-vip-tip strong a, .oo-vip-tip strong a:visited {\n  color: #228be6;\n  text-decoration: underline;\n}\n\n.oo-vip-warning {\n  background-color: #e67700;\n  padding: 0.5em 1em;\n  text-align: center;\n  font-size: 0.85em;\n}\n\n.oo-vip-warning a {\n  color: #fff !important;\n}\n\n.oo-player-box {\n  position: relative;\n  height: 100%;\n  max-height: 100%;\n  border-radius: 2px;\n  overflow: hidden;\n}\n\n.oo-player::before,\n.oo-player::after {\n  display: none !important;\n}\n\n.oo-player-bg {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  background-image: url(").concat(PlayerCover, ");\n  z-index: 2147483646;\n  pointer-events: none;\n}\n\n.oo-video {\n  z-index: 2147483647;\n  position: absolute;\n  top: 0;\n  left: 0;\n  display: block;\n  height: 100%;\n  width: 100%;\n  background-color: black;\n}\n\n.oo-debug-btn {\n  padding: 1px 4px;\n  border-radius: 2px;\n  background-color: rgba(255,255,255,.2);\n  color: rgba(255,255,255,.6);\n}\n"));
        var t = DEBUG || isMobile ? '<div class="oo-vip-panel is-setting">\n<div class="oo-nav-links">\n  <a href="https://greasyfork.org/zh-CN/scripts/393284" target="_blank">油猴地址</a><a href="https://gitee.com/ecruos/oo" target="_blank">代码仓库</a><a href="https://ecruos.gitee.io/one/" target="_blank">One主页</a>\n</div>\n</div>' : "";
        i.eq(0)[o]('\n<div class="oo-vip">\n  <div class="oo-vip-panel">\n    <div class="oo-vip-title">\n      <span class="oo-vip-title-text">'.concat(PLUGIN_NAME, '<span class="oo-vip-small">v').concat(VERSION, "</span></span>\n      ").concat(DEBUG ? '<button class="oo-debug-btn">LOG</button>' : "", '\n    </div>\n    <div class="oo-vip-sign">').concat(OO_SIGN, "</div>\n  </div>").concat(t, '\n  <div class="oo-vip-list').concat(isAllowSniff ? " has-sniff" : "", '">\n<span class="oo-vip-item oo-normal oo-sniff">VIP<span class="oo-vip-weight" style="width:100%;"></span></span>\n').concat(VIP_URLS.map((function(n, o) {
          return '<span class="' + ("oo-vip-item" + (Is(isVipUrlRegex) ? "" : isMobile && n.isM || !isMobile && n.weight >= 1 ? " is-good" : " is-bad") + (n["✔"] ? " is-sniff-source" : "")) + '" _ooKey="' + o + '" _ooWeight="' + n.weight.toFixed(2) + '">' + n.name + '<span class="oo-vip-weight-bg' + (n.isM ? " is-full" : "") + '"></span><span class="oo-vip-weight" style="width:' + n.weight / maxVipWeight * 100 + '%;"></span></span>';
        })).join("\n")).concat(VIP_URLS.length > collapseNum ? '<span class="oo-vip-item oo-normal oo-collapse">➢</span>' : "", "\n  </div>\n  ").concat(isMobile ? '<div class="oo-vip-tip'.concat(isAllowSniff ? "" : " is-warning", '">配合浏览器的<strong>广告拦截</strong>和<strong>嗅探播放</strong>功能使用，体验更佳。').concat(isAllowSniff ? "" : '<br>电脑端支持一键解析嗅探播放功能，移动端已适配<strong>海阔视界</strong>和<strong>米侠</strong>浏览器。<br>可添加 <strong><a href="https://gitee.com/ecruos/oo/raw/master/scripts/oo.movie.adblock.txt" target="_blank">'.concat(PLUGIN_NAME, " adblock</a></strong> 包含的网址来拦截图片广告，或手动拦截。"), "</div>") : "", "\n</div>\n")), 
        smartCollapse(".oo-vip-item"), p();
        var a = 0, r = setInterval((function() {
          p(), a++ > 40 && clearInterval(r);
        }), 250);
      } else setTimeout((function() {
        insertVipSource(n, o, e + 1);
      }), 250);
    }
    function s(n) {
      $(".oo-notification").remove();
      var o = getVipPlayer();
      if (0 !== o.length) {
        pausePlay(), o.empty().append('<div class="oo-player-box"><div class="oo-player-bg"></div><iframe id="oo-player-iframe" style="'.concat("width: 100%; height: 100%; border: none; outline: none; margin: 0; padding: 0; position: absolute; z-index: 2147483647; left: 0;", '" ').concat(' width="100%" height="100%" allowfullscreen="true" allowtransparency="true" frameborder="0" scrolling="no"', ' src="').concat(n, '"></iframe></div>')), 
        $(".oo-player-box").parent().addClass("oo-player");
      } else location.href = n;
    }
    function l(n, o) {
      var e = n + getVipTargetUrl();
      log("click: " + e), $(".oo-vip-item").removeClass("is-active"), $(o).addClass("is-active"), 
      /http:/.test(n) ? (pausePlay(), $("#oo-player-iframe").remove(), setTimeout((function() {
        window.open(e, "_blank");
      }), 100)) : s(e);
    }
    function c(n, o) {
      $(n).off("click").on("click", (function(n) {
        n.preventDefault(), o(this);
      }));
    }
    function p() {
      c(".oo-vip-item:not(.oo-normal)", (async function(n) {
        $(".oo-vip-list.is-sniffing").length > 0 || (await playBefore(), sniffDone(), sniff(++sniffTick), 
        l(VIP_URLS[$(n).attr("_ooKey")].url, n));
      })), c(".oo-vip-list .oo-sniff", (async function() {
        await playBefore(), sniff(++sniffTick, !0), $(".oo-vip-item").removeClass("is-active");
      })), c(".oo-vip-list .oo-collapse", (function(n) {
        $(n).parent(".oo-vip-list").toggleClass("is-open");
      })), c(".oo-vip-sign", (function(n) {
        DEBUG || isMobile ? $(n).parent(".oo-vip-panel").siblings(".is-setting").toggleClass("is-open") : location.href = "https://gitee.com/ecruos/oo";
      })), c(".oo-vip-title-text", (function(n) {
        isMobile ? $(n).parent(".oo-vip-panel").siblings(".is-setting").toggleClass("is-open") : location.href = "https://greasyfork.org/zh-CN/scripts/393284";
      })), c(".oo-debug-btn", (function(n) {
        $("oo-logs").toggleClass("is-active");
      }));
    }
  }
  function callbackWhenDone() {
    $((function() {
      HTML.setAttribute(PLUGIN_ATTR, PLUGIN_ID), setTimeout((function() {
        HTML.removeAttribute(PLUGIN_ATTR);
      }), 3e3);
    }));
  }
  function getSearchSourcesHtml(n) {
    let o = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1];
    var e = Href.match(/\/\/([^\/]+)/)[1];
    return SEARCH_URLS.map((function(o) {
      return o.url.includes(e) ? "" : '<a target="_blank" href="' + toSearchUrl(o.url, n) + '">' + o.name + "</a>";
    })).join("\n") + (o && SEARCH_URLS.length > collapseNum ? '<a class="oo-collapse">➢</a>' : "");
  }
  function insertSearchAddon(n, o) {
    let e = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : "after", i = arguments.length > 3 && void 0 !== arguments[3] ? arguments[3] : 0;
    var t = Href.includes("m.douban.com/search");
    if (!(!n || !t && $(".oo-sources").length > 0 || i > 20)) {
      var a;
      log("insertSearchAddon");
      var r = !1;
      playerTitle = n, "string" == typeof o && (r = o.startsWith("-")) && (o = o.slice(1)), 
      0 !== (a = $(o)).length ? (addCss("\n.oo-sources {\n  max-width: 1000px;\n  margin: 0 auto;\n  padding: 10px;\n}\n\n.oo-vip .oo-sources {\n  max-width: unset;\n  margin-top: 5px;\n}\n\n.oo-sources + .oo-sources,\n.oo-vip-list:not(.has-sniff) .oo-sniff {".concat(PurifyStyle, "}\n\n.oo-sources a {\n  display: inline-flex !important;\n  align-items: center;\n  justify-content: center;\n  border-radius: 4px;\n  font-size: 12px !important;\n  line-height: 1.2;\n  padding: 5px 10px 3px;\n  margin-top: 8px;\n  margin-right: 6px;\n  white-space: nowrap;\n  background-color: #effaf3 !important;\n  color: #257942 !important;\n  cursor: pointer;\n  border: 1px solid #f1f3f5;\n  text-decoration: none;\n  transition: all 0.25s;\n}\n\n.oo-vip-list + .oo-sources .oo-collapse {\n  display: inline-flex !important;\n}\n\n.oo-sources:not(.is-open) .oo-hide {\n  display: none !important;\n}\n\n.oo-collapse {\n  min-width: 2em;\n}\n\n.oo-sources a:hover, .oo-sources a.is-active {\n  border: 1px solid #099268;\n  background-color: #257942 !important;\n  color: #effaf3 !important;\n}\n")), 
      a[r ? "last" : "first"]()[e]('<div class="oo-sources">\n'.concat(getSearchSourcesHtml(purifyKeyword(n), !t), "\n</div>")), 
      t || smartCollapse(".oo-sources a"), $(".oo-sources .oo-collapse").click((function() {
        $(this).parent(".oo-sources").toggleClass("is-open");
      }))) : setTimeout((function() {
        insertSearchAddon(n, o, e, i + 1);
      }), 500);
    }
  }
  function vipAddonGo() {
    let n = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "body";
    function o(o) {
      $(n).addClass("oo").append(o), $(o).attr({
        autoplay: !0,
        controls: !0
      }), o.play(), o.oncanplay = function() {
        o.play();
      };
    }
    addCss("\nbody:after,\n#a1 ~ script ~ div,\n.dplayer > *[id^=ad] {".concat(PurifyStyle, "}\n\n").concat(n, " > video {\n  position: fixed !important;\n  top: 0px !important;\n  left: 0px !important;\n  min-width: 0px !important;\n  min-height: 0px !important;\n  max-width: 99.99% !important;\n  max-height: 99.99% !important;\n  margin: 0px !important;\n  visibility: visible !important;\n  border-width: 0px !important;\n  background: black !important;\n  z-index: 2147483647 !important;\n  width: 100% !important;\n  height: 100% !important;\n}\n\n.oo.is-fail {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center;\n  background-color: #000;\n}\n\n.oo.is-fail .tip {\n  text-align: center;\n  padding: 1em;\n  color: white;\n}\n    ")), 
    $((function() {
      var e, i = !0, t = !1, a = !1, r = setInterval((function() {
        if (!a) if (a = !0, $(n).hasClass("oo")) clearInterval(r); else {
          (e = $("video").get(0)) && o(e);
          var s = $("iframe");
          if (0 !== s.length) {
            for (var l; 0 === (e = s.contents().find("video")).length && (l = s.contents().find("iframe")).length > 0; ) s = l;
            e.length > 0 && e.attr("src") ? (clearInterval(r), i && (i = !0, o(e))) : t && clearInterval(r), 
            a = !1, $("title").html() !== PLUGIN_ID && $("title").html(PLUGIN_ID);
          }
        }
      }), 250);
      setTimeout((function() {
        t = !0;
      }), 15e3);
    }));
  }
  function toShortVideoUrl(n) {
    var o = n.match(shortVideoUrlRegex);
    return o ? o[1] + '<span class="oo-divider">/</span>' + o[3].slice(-15) : n;
  }
  function addIframe(n) {
    var o = document.createElement("iframe");
    return o.src = n, o.frameborder = "0", o.scrolling = "no", $(".oo-iframes").append(o), 
    o;
  }
  function removeIframe() {
    let n = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : ".oo-iframes > iframe";
    var o = "string" == typeof n ? $(n).get(0) : n;
    o.src = "about:blank", $(o).remove();
  }
  function emptyIframes() {
    $(".oo-iframes > iframe").each((function(n, o) {
      removeIframe(o);
    })), $(".oo-iframes").empty();
  }
  function execQuickAddons(n) {
    log("execQuickAddons:", n.name || n.match);
    var o = "";
    n.hide && (o += "\n".concat(n.hide, " {").concat(PurifyStyle, "}\n")), n.css && (o += n.css), 
    o && addCss(o), $((function() {
      if ((n.sign && $(n.sign).html(OO_SIGN), n.vip) && ((Array.isArray(n.vip) ? n.vip : [ n.vip ]).forEach((function(n) {
        insertVipSource((n = n.split(/\s*\|\s*/))[0], n[1]);
      })), n.title)) var o = 0, e = setInterval((function() {
        var i = $(n.title).eq(0).text();
        i && (clearInterval(e), insertSearchAddon(i, ".oo-vip-list", "after")), o++ > 100 && clearInterval(e);
      }), 100);
      n.jump && (log("searchAddon:", n), $((function() {
        var o = "string" == typeof n.keyword ? $(n.keyword).eq(0).text() : "function" == typeof n.keyword ? n.keyword($) : getKeywordFromUrl(n.keyword);
        (Array.isArray(n.jump) ? n.jump : n.jump.split(/\s*,\s*/)).forEach((function(n) {
          n = n.split(/\s*\|\s*/), insertSearchAddon(o, n[0], n[1] || "after");
        }));
      }))), "function" == typeof n.js && n.js();
    }));
  }
  function makeBetterAddons() {
    BETTER_ADDONS.forEach((function(n) {
      ("string" == typeof n.match ? Href.includes(n.match) : Is(n.match)) && execQuickAddons(n);
    }));
  }
  function urlDetector(n) {
    setInterval((function() {
      Href !== window.location.href && (Href = window.location.href, n && n());
    }), 250);
  }
}();